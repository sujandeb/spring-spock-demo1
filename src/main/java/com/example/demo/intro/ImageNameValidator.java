package com.example.demo.intro;

import java.util.Locale;

public class ImageNameValidator {

    public boolean isValidImageExtension(String fileName)
    {
        String lowerCase = fileName.toLowerCase(Locale.ENGLISH);
        return lowerCase.endsWith(".jpeg") || lowerCase.endsWith(".jpg") || lowerCase.endsWith(".png");
    }

}