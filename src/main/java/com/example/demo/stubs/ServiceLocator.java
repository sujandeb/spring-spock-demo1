package com.example.demo.stubs;

public interface ServiceLocator {
	WarehouseInventory getWarehouseInventory();
}
