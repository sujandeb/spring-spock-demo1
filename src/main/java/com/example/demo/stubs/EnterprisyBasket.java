package com.example.demo.stubs;


import com.example.demo.Basket;

public class EnterprisyBasket extends Basket {

	public EnterprisyBasket(ServiceLocator serviceLocator)
	{
		setWarehouseInventory(serviceLocator.getWarehouseInventory());
	}
}
