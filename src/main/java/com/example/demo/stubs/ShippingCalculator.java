package com.example.demo.stubs;

import com.example.demo.Product;

public interface ShippingCalculator {
	int findShippingCostFor(Product product, int times);
}
