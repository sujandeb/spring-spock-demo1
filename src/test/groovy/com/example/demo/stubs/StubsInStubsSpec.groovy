package com.example.demo.stubs

import com.example.demo.Basket
import com.example.demo.Product
import spock.lang.Specification
import spock.lang.Subject

@Subject(EnterprisyBasket.class)
class StubsInStubsSpec extends Specification{

	def "If warehouse is empty nothing can be shipped"() {
		given: "a TV"
		Product tv = new Product(name:"bravia",price:1200,weight:18)
		
		and:"an empty warehouse"
		WarehouseInventory inventory = Stub(WarehouseInventory)
		inventory.isEmpty() >> true
		ServiceLocator serviceLocator = Stub(ServiceLocator)
		serviceLocator.getWarehouseInventory() >> inventory
		
		and: "a basket"
		EnterprisyBasket basket = new EnterprisyBasket(serviceLocator)
		
		when: "user checks out the tv"
		basket.addProduct tv

		then: "order cannot be shipped"
		!basket.canShipCompletely()
	}
	
	
	
}

