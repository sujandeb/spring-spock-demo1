package com.example.demo.blocks

import spock.lang.Specification

class BasicBlocksSpec extends Specification{

	def "Adding two and three results in 5"() {
		given: "the integers two and three"
		int a = 3
		int b = 2

		when: "they are added"
		int result = a + b

		then: "the result is five"
		result == 5
	}
}

