package com.example.demo.intro

import spock.lang.Specification
import spock.lang.Unroll

class ImageNameValidatorTest extends Specification {

    @Unroll("Checking image name #pictureFile")
    def "All kinds of JPEG file are accepted"() {
        given: "an image extension checker"
        ImageNameValidator validator = new ImageNameValidator()

        expect: "that all jpeg filenames are accepted regardless of case"
        validator.isValidImageExtension(pictureFile)

        where: "sample image names are"
        pictureFile << GroovyCollections.combinations([["sample.","Sample.","SAMPLE."],['j', 'J'], ['p', 'P'],['e','E',''],['g','G']])*.join()
    }

}
