package com.example.demo.mocks

import com.example.demo.Basket
import com.example.demo.Product
import com.example.demo.stubs.ShippingCalculator
import com.example.demo.stubs.WarehouseInventory
import spock.lang.Specification
import spock.lang.Subject

@Subject(Basket.class)
class HelperMockingMethodSpec extends Specification{

	def "Only warehouse is queried when checking shipping status"() {
		given: "a basket, a TV and a camera"
		Product tv = new Product(name:"bravia",price:1200,weight:18)
		Product camera = new Product(name:"panasonic",price:350,weight:2)
		Basket basket = new Basket()

		and: "a warehouse with limitless stock"
		WarehouseInventory inventory = Mock(WarehouseInventory)
		basket.setWarehouseInventory(inventory)
		ShippingCalculator shippingCalculator = Mock(ShippingCalculator)
		basket.setShippingCalculator(shippingCalculator)

		when: "user checks out both products"
		basket.addProduct tv
		basket.addProduct camera
		boolean readyToShip = basket.canShipCompletely()

		then: "order can be shipped"
		readyToShip
		interaction { 
			myHelperMethod(inventory,basket ) 
		}
	}

	def myHelperMethod(WarehouseInventory inventory, Basket basket) {
		basket.getProductTypesCount() == 2
		2 * inventory.isProductAvailable( _ , _) >> true
		_ * inventory.isEmpty() >> false
		0 * _
	}
}

